import { Avatar } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../assets/logo.jpg';
import './componentsStyle.css';

export const Gird = (props) => {
    return (

        <div className='gridContainer'>

            {props.data.map(item => {
                return (
                    <Link  key={item.id} to={'/league/' + props.leagueId + '/' + item.id}>
                        <div className='gridItem'>

                            <Avatar src={item.crestUrl || logo} />
                            <h4 className='ItemName'>{item.name}</h4>

                        </div>
                    </Link>)
            })}
        </div>


    )
}