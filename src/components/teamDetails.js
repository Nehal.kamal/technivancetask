import { Breadcrumb, Icon } from 'antd';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { get_team_details } from '../store/actions/index';
import { List } from './list';
import './componentsStyle.css'

class TeamDetails extends Component {
    componentDidMount() {
        this.getTeamDetails()
    }
    calculate_age(dob) {
        var diff_ms = Date.now() - new Date(dob).getTime();
        var age_dt = new Date(diff_ms);

        return Math.abs(age_dt.getUTCFullYear() - 1970);
    }
    getTeamDetails() {
        this.props.onget_teamDetails(this.props.match.params.teamId)
    }
    render() {
        return (
            <div className='compContainer'>
                {this.props.loading && <Icon type="loading" className='loadingIcon' />
                }
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <Link to='/'>Home</Link>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        {this.props.selectedLeague && <Link to={'/league/' + this.props.selectedLeague.id}>
                            {this.props.selectedLeague.name}
                        </Link>
                        }
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        {this.props.selectedTeamDetails && this.props.selectedTeamDetails.name}
                    </Breadcrumb.Item>
                </Breadcrumb>
                <div className='teamsContent'>

                    {this.props.selectedTeamDetails &&
                        <div>
                            <List data={this.props.selectedTeamDetails} showBtn={false} TeamFlag={true} />
                            {this.props.selectedTeamDetails.squad.map(player => {
                                return (<div key={player.id} className='listContainer'>
                                    <Icon type="user" className='userIcon' />
                                    <div className='listItem'>
                                        <h3>
                                            {player.name}
                                        </h3>
                                        {
                                            player.dateOfBirth && <h5>{'Age: ' + this.calculate_age(player.dateOfBirth)}
                                            </h5>}
                                        {player.nationality && <h5>
                                            {'Nationality: ' + player.nationality}
                                        </h5>}
                                        {player.position && <h5>
                                            {'Position: ' + player.position}
                                        </h5>}
                                        {player.shirtNumber && <h5>
                                            {'Shirt Number: ' + player.shirtNumber}
                                        </h5>}
                                    </div>

                                </div>
                                )
                            })}
                        </div>}

                </div>
            </div>

        )
    }
}


const mapStateToProps = state => {
    return {
        loading: state.leagues.loading,
        selectedLeague: state.leagues.selectedLeague,
        selectedTeamDetails: state.leagues.selectedTeamDetails
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onget_teamDetails: (id) => dispatch(get_team_details(id)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamDetails);
