import { Breadcrumb, Icon } from 'antd';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getCompDetails, get_teams } from '../store/actions/index';
import { Gird } from './grid';
import { List } from './list';
import './componentsStyle.css'

class Teams extends Component {
    componentDidMount() {
        this.getCompDetails();
        this.getTeams()
    }
    getCompDetails() {
        // this.props.selectedLeague.id
        this.props.ongetCompDetails('2021')
    }
    getTeams() {
        // this.props.selectedLeague.id
        this.props.onget_teams('2021')
    }

    render() {
        return (
            <div className='compContainer'>
                {this.props.loading && <Icon type="loading" className='loadingIcon'/>
                }
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <Link to='/'>Home</Link>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        {this.props.selectedLeague && this.props.selectedLeague.name}

                    </Breadcrumb.Item>
                </Breadcrumb>
                <div className='teamsContent'>

                    {this.props.selectedLeague &&

                        <List data={this.props.selectedLeague} showBtn={false} />}
                    {
                        (this.props.teams && this.props.selectedLeague) && <Gird data={this.props.teams} leagueId={this.props.selectedLeague.id} />
                    }
                </div>
            </div>

        )
    }
}


const mapStateToProps = state => {
    return {
        loading: state.leagues.loading,
        selectedLeague: state.leagues.selectedLeague,
        teams: state.leagues.teams
    }
}

const mapDispatchToProps = dispatch => {
    return {
        ongetCompDetails: (id) => dispatch(getCompDetails(id)),
        onget_teams: (id) => dispatch(get_teams(id)),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Teams);
